import { Injectable } from "@angular/core";

import { Item } from "./item";

@Injectable()
export class ItemService {
    private items = new Array<Item>(
        { id: 1, name: "Ter Stegen", role: "Goalkeeper", src: "http://images.performgroup.com/di/library/goal_es/ab/d9/marc-andre-ter-stegen-barcelona-atletico-madrid-la-liga_9aaiu7wl2amh1mdgze6xh04gf.jpg?t=1318343271&w=620&h=430" },
        { id: 3, name: "Piqué", role: "Defender", src: "http://images.performgroup.com/di/library/omnisport/aa/d1/gerard-pique-cropped_3y2xyriyhrae17nz0ntdqsraf.jpg?t=-13063301&w=620&h=430" },
        { id: 4, name: "I. Rakitic", role: "Midfielder", src: "http://i2.mirror.co.uk/incoming/article8887608.ece/ALTERNATES/s615/FC-Barcelona-v-Club-Atletico-de-Madrid-La-Liga.jpg" },
        { id: 5, name: "Sergio", role: "Midfielder", src: "http://i.dailymail.co.uk/i/pix/2015/06/29/14/29F5CC7900000578-0-image-a-2_1435584845697.jpg" },
        { id: 6, name: "Denis Suárez", role: "Midfielder", src: "http://a.espncdn.com/combiner/i/?img=/photo/2017/0126/r175419_1296x729_16-9.jpg&w=738&site=espnfc" },
        { id: 7, name: "Arda", role: "Midfielder", src: "https://www.thesun.co.uk/wp-content/uploads/2016/08/nintchdbpict0002555935391.jpg?w=960&strip=all" }
        /*
        { id: 8, name: "A. Iniesta", role: "Midfielder" },
        { id: 9, name: "Suárez", role: "Forward" },
        { id: 10, name: "Messi", role: "Forward" },
        { id: 11, name: "Neymar", role: "Forward" },
        { id: 12, name: "Rafinha", role: "Midfielder" },
        { id: 13, name: "Cillessen", role: "Goalkeeper" },
        { id: 14, name: "Mascherano", role: "Defender" },
        { id: 17, name: "Paco Alcácer", role: "Forward" },
        { id: 18, name: "Jordi Alba", role: "Defender" },
        { id: 19, name: "Digne", role: "Defender" },
        { id: 20, name: "Sergi Roberto", role: "Midfielder" },
        { id: 21, name: "André Gomes", role: "Midfielder" },
        { id: 22, name: "Aleix Vidal", role: "Midfielder" },
        { id: 23, name: "Umtiti", role: "Defender" },
        { id: 24, name: "Mathieu", role: "Defender" },
        { id: 25, name: "Masip", role: "Goalkeeper" }
        */
    );
    getItems(): Item[] {
        return this.items;
    }

    getItem(id: number): Item {
        return this.items.filter(item => item.id === id)[0];
    }
}
