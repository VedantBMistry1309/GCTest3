import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ObservableArray, ChangedData, ChangeType } from "tns-core-modules/data/observable-array";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Page } from "ui/page";
import { View } from "ui/core/view";
import { Color } from"color";
import { ScrollEventData, ScrollView } from "ui/scroll-view";

import { Item } from "./item";
import { ItemService } from "./item.service";

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "items.component.html",
    styleUrls: ["items.component.css"]
})
export class ItemsComponent implements OnInit {
    items: ObservableArray<Item>;
    status: string;
    show: boolean;
    limit: number;
    showAnim: boolean;
    @ViewChild("searchBox") searchBox: ElementRef;
    @ViewChild("searchAction") searchAction: ElementRef;
    //@ViewChild("scrollView") scrollView: ElementRef;

    constructor(private itemService: ItemService, private page: Page) {
        this.show = false;
        this.showAnim = true;
        this.limit = 240;
    }

    ngOnInit(): void {
        this.page.actionBarHidden = true;
        this.page.backgroundColor = new Color("#e0e0e0");
        this.items = new ObservableArray(this.itemService.getItems());
        console.log("Items Loaded");
    }

    public onScroll(args: ScrollEventData) {
        this.status = "scrolling";
        let searchBox = <View> this.searchBox.nativeElement;
        let searchAction = <View>this.searchAction.nativeElement;
        let that = this;
        setTimeout(function () {
            that.status = "not scrolling";
        }, 300);
        if (args.scrollY > this.limit) {
            this.show = true;
            if (this.showAnim) {
                searchAction.animate({
                    opacity: 0,
                    duration: 0
                });
                searchAction.animate({
                    opacity: 1,
                    duration: 400
                });
                this.showAnim = false;
            }
            searchBox.animate({
                opacity: 0,
                duration: 400
            });
            console.log("Showing...");
        } else if (args.scrollY < this.limit) {
            this.show = false;
            if (!this.showAnim) {
                searchAction.animate({
                    opacity: 0,
                    duration: 0
                });
                searchAction.animate({
                    opacity: 1,
                    duration: 400
                });
                this.showAnim = true;
            }
            searchBox.animate({
                opacity: 1,
                duration: 400
            });
            console.log("Hiding Textfield...");
        }
        console.log("scrollX: " + args.scrollX);
        console.log("scrollY: " + args.scrollY);
    }
}
